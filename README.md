# OpenML dataset: ipums_la_99-small

https://www.openml.org/d/378

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: IPUMS (ipums@hist.umn.edu)  
**Donor**: Stephen Bay (sbay@ics.uci.edu)  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/IPUMS+Census+Database) - 1999   
**Please cite**:   

**IPUMS Database**  
This data set contains unweighted PUMS census data from the Los Angeles and Long Beach areas for the years 1970, 1980, and 1990. The coding schemes have been standardized (by the IPUMS project) to be consistent across years. The original source for this data set is the IPUMS project (RugglesSobek, 1997). The IPUMS project is a large collection of federal census data which has standardized coding schemes to make comparisons across time easy.

The data is an unweighted 1 in 100 sample of responses from the Los Angeles -- Long Beach area for the years 1970, 1980, and 1990. The household and individual records were flattened into a single table and we used all variables that were available for all three years. When there was more than one version of a variable, such as for race, we used the most general. For occupation and industry we used the 1950 basis.

Note that PUMS data is based on cluster samples, i.e. samples are made of households or dwellings from which there may be multiple individuals. Individuals from the same household are no longer independent. Ruggles (1995) considers this issue further and discusses its effect (along with the effects of stratification) on standard errors.

The variable schltype appears to have different coding values across the years 1970, 1980, and 1990.

There are two versions of this data set. The small data set contains a 1 in 1000 sample of the Los Angeles and
Long Beach area. It was formed by sampling from the large data set. The large data set contains a 1 in 100 sample of the Los Angeles and Long Beach area.

**Past Usage**  
S. D. Bay and M. J. Pazzani. (1999) "Detecting Group Differences: Mining Contrast Sets". submitted.

**Copyright Information**  
All persons are granted a limited license to use and distribute this documentation and the accompanying data, subject to the following conditions:
* No fee may be charged for use or distribution.
* Publications and research reports based on the database must cite it appropriately. The citation should include the following: Steven Ruggles and Matthew Sobek et. al. Integrated Public Use Microdata Series: Version 2.0 Minneapolis: Historical Census Projects, University of Minnesota, 1997

If possible, citations should also include the URL for the IPUMS site: http://www.ipums.umn.edu/.

In addition, we request that users send us a copy of any publications, research reports, or educational material making use of the data or documentation. Send all electronic material to ipums@hist.umn.edu

References  

1. http://www.ipums.umn.edu/
2. mailto:ipums@hist.umn.edu
3. http://www.ics.uci.edu/~sbay
4. mailto:sbay@ics.uci.edu
5. http://www.ipums.umn.edu/
6. mailto:ipums@hist.umn.edu
7. http://www.ipums.umn.edu/
8. http://www.census.gov/
9. http://kdd.ics.uci.edu/
10. http://www.ics.uci.edu/
11. http://www.uci.edu/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/378) of an [OpenML dataset](https://www.openml.org/d/378). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/378/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/378/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/378/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

